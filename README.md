# LaTex on Gitlab

This template allows for automatic online compilation of your LaTeX project after every git commit.

#### [PDF are served here](https://priesemann-group.pages.gwdg.de/tex-template)
(adjust the Readme after cloning)

#### Warning: Access control for pages is not enabled by gwdg (yet). PDFs are accessible online by anyone who guesses the URL. I will try to get this fixed asap.



### Pros
- Version control based on git
    + Branch, merge, revert
- Work offline, push later
- Customizable
    + Full latex environment
    + Install custom packages
    + Make, Multistep building
- Uses Docker
    + you can run the same setup on your computer (todo, see below)
    + [pre-built container](https://github.com/blang/latex-docker)
- LaTeX Diff
    + see also this great [git latexdiff utility](https://gitlab.com/git-latexdiff/git-latexdiff)

### Cons
- Version control based on git
    + Compilation is triggered only after committing.
    + Some [style recommendations](https://stackoverflow.com/a/6190412)
        * One sentence per line.
- When working exclusively online: hard to debug faulty tex commands.


### Alternatives
- [Overleaf](https://www.overleaf.com/)
    + Feels like WSIWG
    + Collaborate in real-time (see your coworker's cursor)
    + Git integration (manual, paid)

## Setup

All required resources are contained in the `web` folder and `.gitlab-ci.yml` that configures the building pipeline.
For an existing project (on gitlab), copy both into the root directory and enable pipelines in the project settings. For the next commit, everything should be ready and files are served on the projects gitlab pages.

#### To start a new Project

- Create a new project on gitlab
    + `Import Project > Repy by URL`

- Enable Pipelines
    + `Settings > General > Visibility, project features, permissions > Pipelines`

- Make a commit

- Get the URL of the gitlab pages
    + `Settings > Pages`
    + Only be available, after the pipeline has run at least once.
    + Copy the link, paste in the README of your project or save somewhere handy.
    + For gwdg, they usually have the form `https://username.pages.gwdg.de/projectname`




