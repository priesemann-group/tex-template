#!/bin/bash

GITTIME=$(git log -n 1 --pretty=%cd --date=format:%y-%m-%d_%H-%M-%S)
GITDAY=$(git log -n 1 --pretty=%cd --date=format:%Y-%m-%d)
GITSHA=$(git log -n 1 --pretty=%H)
GITSHASHORT=$(git log -n 1 --pretty=%h)

# make sure the script is run from the root project directory!
source web/config.sh

mkdir -p build_main/$GITSHA
$TEXCMD
mv build_main/$GITSHA/$MAINTEX.pdf build_main/$GITSHA/$FILEPTRN.pdf
mv build_main/$GITSHA/$MAINTEX.log build_main/$GITSHA/$FILEPTRN.log

