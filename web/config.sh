#!/bin/bash

# give the name of the root tex file without.tex ending
MAINTEX="main"

# specify the latex command to use for compiling.
# do not change the output-directory.
TEXCMD="latexmk -f -xelatex -interaction=nonstopmode -synctex=1 --output-directory=./build_main/$GITSHA $MAINTEX.tex"

# How many diffs to create
NUMDIFF=2

# How many past commits (versions) to include in the html
MAXDIFF=15

# in the future: specify the naming scheme for generated pdf files.
# do not change this for now, it is not respected everywhere.
FILEPTRN="$CI_PROJECT_NAME"_$GITTIME
DIFFPTRN="$CI_PROJECT_NAME"_diff
