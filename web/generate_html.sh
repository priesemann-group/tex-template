#!/bin/bash

GITTIME=$(git log -n 1 --pretty=%cd --date=format:%y-%m-%d_%H-%M-%S)
GITDAY=$(git log -n 1 --pretty=%cd --date=format:%Y-%m-%d)
GITSHA=$(git log -n 1 --pretty=%H)
GITUSR=$(git log -n 1 --pretty=%an)
GITSHASHORT=$(git log -n 1 --pretty=%h)

# make sure the script is run from the root project directory!
source web/config.sh

mkdir -p public/build_main
mkdir -p public/build_diff
cp -rf build_main/* public/build_main/
cp -rf build_diff/* public/build_diff/
# cp -rf build_prev/public/build_main/* public/build_main/

# latest version variables
MAINPATH=build_main/$GITSHA/$FILEPTRN
GITMSG=$(git log -n 1 --pretty=%B)
GITLOG=$(git log -n 1)

cp $MAINPATH.pdf "public/latest.pdf"
cd "public"
INDEX="index.html"

echo "Variables are set"

# header
cat << EOF > $INDEX
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>$CI_PROJECT_NAME</title>
  </head>

<body>
<main role="main">
<section class="jumbotron jumbotron-fluid text-center">
    <div class="container">
      <h1 class="jumbotron-heading">$CI_PROJECT_NAME</h1>
      <!-- <p class="lead text-muted">Project Description</p> -->
      <p>
        <a href="latest.pdf" class="btn btn-primary my-2">Latest PDF</a>
        <a href="$CI_PROJECT_URL" class="btn btn-secondary my-2">Go to project</a>
      </p>
    </div>
</section>

<div class="container-fluid">
<div class="row mb-4">
<div class="col-12">
EOF

# Latest Version
echo "Latest Version"
cat << EOF >> $INDEX
<div class="card">
<h4 class="card-header">Versions</h4>
<ul class="list-group list-group-flush">
    <li class="list-group-item">
        <div class="d-flex align-content-center">
        <a href="$MAINPATH.pdf" class="btn btn-sm btn-primary py-1 d-flex align-content-center"><div style="margin-top:.125rem">PDF</div></a>
        <button class="btn btn-sm btn-primary py-1 ml-2 d-flex align-content-center" disabled><div style="margin-top:.125rem">Diff</div></button>
        <h6 class="card-subtitle text-muted text-truncate ml-2 mt-2 mb-2">$GITDAY</h6>
        <div class="d-inline-block text-truncate ml-5" style="max-width: 30%; margin-top:.35rem;">$GITMSG</div>
        <h6 class="card-subtitle text-muted text-truncate ml-auto mr-2 mt-2 mb-2">${GITUSR}</h6>
        <button class="btn btn-sm btn-primary ml-2 py-1" data-toggle="collapse" href="#g0" role="button" aria-expanded="false" aria-controls="g0">$GITSHASHORT</button>
        <button class="btn btn-sm btn-primary ml-2 py-1" data-toggle="collapse" href="#l0" role="button" aria-expanded="false" aria-controls="l0">Tex</button>
        </div>
        <div class="collapse" id="g0">
            <pre class="card card-body p-2 mb-0 mt-2">$GITLOG</pre>
        </div>
        <div class="collapse" id="l0">
            <pre class="card card-body p-2 mb-0 mt-2">$(cat $MAINPATH.log)</pre>
        </div>
    </li>
EOF

# git versions
for (( i = 0; i < $MAXDIFF; i++ )); do
    if [ $((i+1)) -gt $(git log --pretty=format:'' | wc -l) ]; then
        break
    fi
TARSHA=$(git log -n $((i+2)) --pretty=%H | tail -n 1)
TARSHASHORT=$(git log -n $((i+2)) --pretty=%h | tail -n 1)
TARMSG=$(git show --pretty="%B" -s $TARSHA)
TARLOG=$(git show -s $TARSHA)
TARUSR=$(git show -s $TARSHA --pretty=%an)
TARDAY=$(git show --pretty=%cd --date=format:%Y-%m-%d -s $TARSHA)
TARTIME=$(git show --pretty=%cd --date=format:%y-%m-%d_%H-%M-%S -s $TARSHA)
# hardcoding path here is not ideal, should be done via config.
TARPATH="build_main/$TARSHA/$CI_PROJECT_NAME"_"$TARTIME"
TARDIFF="build_diff/$GITSHA/$DIFFPTRN"_"$GITSHASHORT"_"$TARSHASHORT"

# try to get old pdfs from previous gitlab pages. structure should match!
mkdir -p "build_main/$TARSHA/"

if curl --output /dev/null --silent --head --fail "$CI_PAGES_URL/$TARPATH.pdf"; then
    curl $CI_PAGES_URL/$TARPATH.pdf --silent --output "$TARPATH.pdf"
    curl $CI_PAGES_URL/$TARPATH.log --silent --output "$TARPATH.log"
    echo "$TARPATH copied from pages"
# wget $CI_PAGES_URL/$TARPATH.pdf -O "$TARPATH.pdf"
# wget $CI_PAGES_URL/$TARPATH.log -O "$TARPATH.log"
else
    echo "$TARPATH not found"
fi


cat << EOF >> $INDEX
    <li class="list-group-item">
        <div class="d-flex align-content-center">
EOF

# main file button
if [ -f "$TARPATH.pdf" ]; then
cat << EOF >> $INDEX
        <a href="$TARPATH.pdf" class="btn btn-sm btn-secondary py-1 d-flex align-content-center"><div style="margin-top:.125rem">PDF</div></a>
EOF
else
cat << EOF >> $INDEX
        <button href="" class="btn btn-sm btn-secondary py-1 d-flex align-content-center" disabled ><div style="margin-top:.125rem">PDF</div></button>
EOF
fi

# diff file button

if [ -f "$TARDIFF.pdf" ]; then
cat << EOF >> $INDEX
        <a href="$TARDIFF.pdf" class="btn btn-sm btn-secondary py-1 ml-2 d-flex align-content-center"><div style="margin-top:.125rem">Diff</div></a>
EOF
else
cat << EOF >> $INDEX
        <button class="btn btn-sm btn-secondary py-1 ml-2 d-flex align-content-center" disabled><div style="margin-top:.125rem">Diff</div></button>
EOF
fi

cat << EOF >> $INDEX
        <h6 class="card-subtitle text-muted text-truncate ml-2 mt-2 mb-2">$TARDAY</h6>
        <div class="d-inline-block text-truncate ml-5" style="max-width: 30%; margin-top:.35rem;">$TARMSG</div>
        <h6 class="card-subtitle text-muted text-truncate ml-auto mr-2 mt-2 mb-2">${TARUSR}</h6>
        <button class="btn btn-sm btn-secondary ml-2 py-1" data-toggle="collapse" href="#g$((i+1))" role="button" aria-expanded="false" aria-controls="g$((i+1))">$TARSHASHORT</button>
EOF

if [ -f "$TARPATH.log" ]; then
cat << EOF >> $INDEX
        <button class="btn btn-sm btn-secondary ml-2 py-1" data-toggle="collapse" href="#l$((i+1))" role="button" aria-expanded="false" aria-controls="l$((i+1))">Tex</button>
        </div>
        <div class="collapse" id="g$((i+1))">
            <pre class="card card-body p-2 mb-0 mt-2">$TARLOG</pre>
        </div>
        <div class="collapse" id="l$((i+1))">
            <pre class="card card-body p-2 mb-0 mt-2">$(cat $TARPATH.log)</pre>
        </div>
    </li>
EOF
else
cat << EOF >> $INDEX
        <button class="btn btn-sm btn-secondary ml-2 py-1" disabled data-toggle="collapse" href="#l$((i+1))" role="button" aria-expanded="false" aria-controls="l$((i+1))">Tex</button>
        </div>
        <div class="collapse" id="g$((i+1))">
            <pre class="card card-body p-2 mb-0 mt-2">$TARLOG</pre>
        </div>
        <div class="collapse" id="l$((i+1))">
        </div>
    </li>
EOF
fi

done

# footer
echo "Footer"
cat << EOF >> $INDEX
</ul>
</div>

</div>
</div>
</div>
</main>
</body>
</html>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
EOF

cd ".."
